import SocketIOClient from 'socket.io-client';
import config from './../config';

export default (async function() {
    console.log('start connection with websocket');

    let socket = SocketIOClient(config.socket_url);

    socket.on('connect', function() {
        console.log('Connected with websocket');
    });

    return socket;
});