import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome'

export default class Link extends Component {
    render() {
        return <div className="App-link">
            <span className="fa-stack fa-lg">
                <FontAwesome name="square" className="fa-stack-2x"/>
                <FontAwesome name={this.props.icon} className="fa-stack-1x fa-inverse"/>
            </span>
            <a href={this.props.link} target="_blank">{this.props.name}</a>
        </div>
    }
}
