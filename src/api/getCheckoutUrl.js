import config from './../config'

export default (async function getCheckoutUrl() {
    try {
        let response = await fetch(config.api + '/checkout/url');
        let responseJson = await response.json();

        if (responseJson.success) {
            return responseJson.url;
        }
    } catch(error) {
        console.error(error);
    }
});
