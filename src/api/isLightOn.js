import config from './../config'

export default (async function isLightOn() {
    try {
        let response = await fetch(config.api);
        let responseJson = await response.json();

        return responseJson.state === 'ON';
    } catch(error) {
        console.error(error);
    }
});
