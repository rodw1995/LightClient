import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import FontAwesome from 'react-fontawesome'
import connectWithSocket from './websocket/connect';
import isLightOn from './api/isLightOn';
import getCheckoutUrl from './api/getCheckoutUrl';
import './App.css';
import config from './config';
import Link from './Link';
import react_logo from './assets/react.png';
import nodejs_logo from './assets/nodejs.png';
import arduino_logo from './assets/arduino.png';

import {ToastContainer, ToastMessage} from 'react-toastr';
const ToastMessageFactory = React.createFactory(ToastMessage.animation);

class App extends Component {
    state = {
        isLightOn: false,
    };

    render() {
        return (
            <div className="App">
                <div className="App-content-wrapper">
                    <div className="App-header">
                        <h2>MyLight</h2>
                    </div>
                    <div className="App-content">
                        <p style={{margin: 30}}>Current light state:
                            {this.state.isLightOn ?
                                <span className="state" style={{color: '#37D67A'}}>ON</span> :
                                <span className="state" style={{color: '#F47373'}}>OFF</span>
                            }
                        </p>
                        <button className="checkout" onClick={this._handleCheckoutClick}>Buy light time with Gulden</button>
                        <p>OR</p>
                        <QRCode value={config.buy_token} size="256"/>
                        <p>Scan the code with the app and support the light with 5 free minutes!</p>
                    </div>
                    <div className="App-links">
                        <div className="App-link">
                            <span className="fa-stack fa-lg">
                                <FontAwesome name="link" className="fa-stack-1x"/>
                            </span>
                            <h3>Links</h3>
                        </div>
                        <Link icon="gitlab" name="NodeJS Server" link="https://gitlab.com/rodw1995/LightServer"/>
                        <Link icon="gitlab" name="React Client" link="https://gitlab.com/rodw1995/LightClient"/>
                        <Link icon="gitlab" name="React Native App" link="https://gitlab.com/rodw1995/LightApp"/>
                        <Link icon="gitlab" name="Arduino Project" link="https://gitlab.com/rodw1995/LightArduino"/>
                        <Link icon="mobile" name="Download with Expo" link="https://expo.io/@rodw1995/my-light"/>
                        <Link icon="android" name="Android APK" link="MyLight.apk"/>
                    </div>
                </div>
                <div className="App-footer">
                    <div className="wrapper">
                        <div className="box"><img src={react_logo} alt="react" /></div>
                        <div className="box"><img src={nodejs_logo} alt="nodejs" /></div>
                        <div className="box"><img src={arduino_logo} alt="arduino" /></div>
                    </div>
                </div>
                <ToastContainer
                    toastMessageFactory={ToastMessageFactory}
                    ref="container"
                    className="toast-top-right"
                />
            </div>
        );
    }

    componentDidMount() {
        this._setLightOnState();
        this._setUpWebSocket();
    }

    _setLightOnState = async () => {
        this.setState({
            isLightOn: await isLightOn(),
        });
    };

    _setUpWebSocket = async () => {
        this.socket = await connectWithSocket();
        this.socket.on('light_changed', (data) => {
            if (this.state.isLightOn !== data.light.on) {
                this.setState({
                    isLightOn: data.light.on,
                });

                if (data.light.on === true) {
                    this.refs.container.success('The light is switched on!', 'Light switched', {
                        timeOut: 10000
                    });
                } else {
                    this.refs.container.error('The light is switched off!', 'Light switched', {
                        timeOut: 10000
                    });
                }
            } else if (this.state.isLightOn && data.light.on) {
                this.refs.container.success('Someone bought some extra light time, we appreciate it!', 'More light', {
                    timeOut: 10000
                });
            }
        });
    };

    _handleCheckoutClick = async (e) => {
        e.preventDefault();

        window.location.href = await getCheckoutUrl();
    };
}

export default App;
